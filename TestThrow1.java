package StingsExxample;

public class TestThrow1 {
    //function to check if person is eligible to vote or not
   /*public static void validate(int age) {
        if (age < 18) {
            //throw Arithmetic exception if not eligible to vote
            throw new ArithmeticException("Person is not eligible to vote");
        } else {
            System.out.println("Person is eligible to vote!!");
        }
    }
*/
    //0 1 // size creation index starts from 0


    //main method
    public static void main(String args[]) {
        //calling the function
  /*      validate(13);
        System.out.println("rest of the code...");
   */
        int a[]=new int[5];//declaration and instantiation
        a[0]=10;//initialization
        a[1]=20;
        a[2]=70;
        a[3]=40;
        a[4]=50;
        String a1[]=new String[1];
        a1[0]="java";
        for(int i=0;i<a1.length;i++){
            System.out.println(a1[i]);
            //i=0,0<1,a1[0]="java"
            //i=1,1<1

        }
    //0,4,9

//          STEP1      STEP2    STEP4
        for(int i=0;i<a.length;i++)//length is the property of array
                    //STEP3 ( ONLY IF STEP 2 GETS TRUE IT WILL ENTER INTO STEP3
            System.out.println(a[i]);
    }
    // i=0,0<5,a[0]=10,i++=(0+1)
    //i=1 ,1<5,a[1]=20,i++=(1+1)
    //i=2,2<5,a[2]=70,i++=(2+1)
    //i=3,3<5,a[3]=40,i++=(3+1)
    //i=4,4<5,a[4]=50,i++=(4+1)
    //i=5,5<5

}
