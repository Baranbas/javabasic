package StingsExxample;

public class GFG {
    /*Mutability Difference:

String is immutable, if you try to alter their values, another object gets created, whereas StringBuffer and StringBuilder are mutable so they can change their values.

Thread-Safety Difference:

The difference between StringBuffer and StringBuilder is that StringBuffer is thread-safe. So when the application needs to be run only in a single thread then it is better to use StringBuilder. StringBuilder is more efficient than StringBuffer.

*/


        // Method 1
        // Concatenates to String
        public static void concat1(String s1)
        {
            s1 = s1 + "string";  //"geeks"+"forggeeks"
        }

        // Method 2
        // Concatenates to StringBuilder
        public static void concat2(StringBuilder s2)
        {
            s2.append("StringBuilder");  //geeks.append("forgeegks)
        }

        // Method 3
        // Concatenates to StringBuffer
        public static void concat3(StringBuffer s3)
        {
            s3.append("stringbuffeer");
        }

        // Method 4
        // Main driver method
        public static void main(String[] args)
        {
            // Custom input string
            // String 1

            String s11="Sachin";
            String s12="Sachin";
            String s13="Sachin";


            System.out.println(+ System.identityHashCode(s11));
            System.out.println(+ System.identityHashCode(s12));
            String s1 = "Geeks";  //immutatble   POOL MEMORY
            //String s2 = "Geeks";   //POOL MEMORY


            // Calling above defined method
            GFG.concat1(s1);
            // s1 is not changed
            System.out.println("String: " + s1);

            // String 1
            StringBuilder s2 = new StringBuilder("Geeks");  //mutable

            // Calling above defined method
            GFG.concat2(s2);

            // s2 is changed
            System.out.println("StringBuilder: " + s2);

            // String 3
            StringBuffer s3 = new StringBuffer("Geeks");

            // Calling above defined method
            GFG.concat3(s3);

            // s3 is changed
            System.out.println("StringBuffer: " + s3);
        }
    }

