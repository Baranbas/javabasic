package IOpackage;

import java.io.*;

public class BufferedReaderEx {
    public static  void main(String arg[]){

        try {

            FileReader fileReader = new FileReader("D:\\input1.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);



            FileWriter filewriter = new FileWriter("D:\\outfile1.txt");
            // Connect the FileReader to the BufferedReader
            BufferedWriter bufferedWriter =new BufferedWriter(filewriter);

            String line;
            while(( line= bufferedReader.readLine()) != null) {  //(line=null) !=null  false
                System.out.println(line); // Display the file's contents on the screen, one line at a time
                bufferedWriter.write(line);

            }

            bufferedReader.close(); // Close the stream
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



}
