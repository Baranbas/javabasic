package StingsExxample;



public class Stringpool {
    public static void main(String[] args)
    {
        String s1 = "Java";
        String s2 = "Java";
        String s3 = new String("Java");
        String s4 = new String("Java").intern();
        String s="SachinTendulkar";
        System.out.println(s.substring(8));
        System.out.println(s.substring(1,8));
       String s8= s.concat("tendualkar");
        System.out.println(s8+"   "+s);

        System.out.println((s1 == s2)+", String are equal."); // true  == method address
        System.out.println((s1 == s3)+", String are not equal."); // false
        System.out.println((s1 == s4)+", String are equal."); // true
        System.out.println((s2 == s4)+", String are equal."); // true
        System.out.println(s1.equals(s3));   //equal for content check

        String s11="Sachin"; //83
        String s12="Sachin";//83
        String s13="Ratan";  //82
        System.out.println(s11.compareTo(s12));//0
        System.out.println(s11.compareTo(s13));//1(because s1>s3)
        System.out.println(s13.compareTo(s11));//-1(because s3 < s1 )

    }

}
